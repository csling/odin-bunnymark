# Odin Bunnymark

This is just a reimplementation of Raylib's "bunnymark" example in Odin rather than C, but with an added GameWindow structure that can be used for upscaling when the window is resized.