package main

import rl "vendor:raylib"

MAX_BUNNIES         :i32: 50000
MAX_BATCH_ELEMENTS  :i32: 8192

BunnyGame :: struct {
    quit:       bool,
    window:     GameWindow,
    bunnies:    []Bunny,
    bunCount:   i32,
    bunTexture: rl.Texture,
}

Bunny :: struct {
    position:   rl.Vector2,
    speed:      rl.Vector2,
    color:      rl.Color,
}

InitBunnyGame :: proc(gw: GameWindow) -> BunnyGame {
    bunnies := make([]Bunny, MAX_BUNNIES)
    bunTexture := rl.LoadTexture("resources/wabbit_alpha.png")
    return BunnyGame{false, gw, bunnies, 0, bunTexture}
}

CleanUpGame :: proc(g: BunnyGame) {
    rl.UnloadTexture(g.bunTexture)
    delete(g.bunnies)
}

Update :: proc(g: ^BunnyGame) {
    if rl.WindowShouldClose() {
        g.quit = true
    }

    rl.SetMouseScale(ScalingFactorX(g.window), ScalingFactorY(g.window))

    if(rl.IsMouseButtonDown(rl.MouseButton.LEFT)) {
        for i :i32 = 0; i < 100; i += 1 {
            if(g.bunCount < MAX_BUNNIES) {
                position := rl.GetMousePosition()
                speed := rl.Vector2{f32(rl.GetRandomValue(-250, 250)) / 60.0, f32(rl.GetRandomValue(-250, 250)) / 60.0}
                color := rl.Color {
                    u8(rl.GetRandomValue(50, 240)), 
                    u8(rl.GetRandomValue(80, 240)), 
                    u8(rl.GetRandomValue(100, 240)), 
                    255 }

                g.bunnies[g.bunCount] = Bunny{position, speed, color}
                g.bunCount += 1
            }
        }
    }

    for i :i32 = 0; i < g.bunCount; i += 1 {
        bun := &g.bunnies[i]
        
        bun.position.x += bun.speed.x
        bun.position.y += bun.speed.y

        halfTW := g.bunTexture.width / 2
        halfTH := g.bunTexture.height / 2

        if((i32(bun.position.x) + halfTW) > WINDOW_W || (i32(bun.position.x) + halfTW) < 0) {
            bun.speed.x *= -1
        }
        if((i32(bun.position.y) + halfTH) > WINDOW_H || (i32(bun.position.y) + (halfTH - 40)) < 0) {
            bun.speed.y *= -1
        }
    }
}

Draw :: proc(g: ^BunnyGame) {
    BeginWindowTextureMode(g.window)
        rl.ClearBackground(rl.RAYWHITE)

        for i :i32 = 0; i < g.bunCount; i += 1 {
            bun := g.bunnies[i]
            rl.DrawTextureEx(g.bunTexture, bun.position, 0., 1., bun.color)
        }
        
        rl.DrawRectangle(0, 0, WINDOW_W, 40, rl.BLACK)
        rl.DrawText(rl.TextFormat("bunnies: %i", g.bunCount), 120, 10, 20, rl.GREEN)
        rl.DrawText(rl.TextFormat("batched draw calls: %i", 1 + g.bunCount / MAX_BATCH_ELEMENTS), 320, 10, 20, rl.MAROON)
        rl.DrawFPS(10, 10)
    EndWindowTextureMode()
}

GameLoop :: proc(g: ^BunnyGame) {
    defer(rl.UnloadTexture(g.bunTexture))
    for !g.quit {
        Update(g)
        Draw(g)
        DrawWindow(g.window)
    }
}