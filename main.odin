package main

import rl "vendor:raylib"

WINDOW_W, WINDOW_H :: 800, 450

main :: proc() {
    gw := InitGameWindow(WINDOW_W, WINDOW_H, "raylib [bunnymark w/ scalable window] - ODIN", 60)
    defer DeinitGameWindow(gw)

    game := InitBunnyGame(gw)
    defer CleanUpGame(game)

    GameLoop(&game)
}