package main

import rl "vendor:raylib"

GameWindow :: struct {
    width:      i32,
    height:     i32,
    texture:    rl.RenderTexture,
    screenRect: rl.Rectangle,
}

InitGameWindow :: proc(w: i32, h: i32, title: cstring, fpsCap: i32) -> GameWindow {
    rl.SetConfigFlags(rl.ConfigFlags{rl.ConfigFlag.WINDOW_RESIZABLE})
    rl.InitWindow(w, h, title)
    rl.SetTargetFPS(fpsCap)

    tex := rl.LoadRenderTexture(w, h)
    rect := rl.Rectangle{0., 0., f32(w), -f32(h)}
    return GameWindow{w, h, tex, rect}
}
DeinitGameWindow :: proc(w: GameWindow) {
    rl.UnloadRenderTexture(w.texture)
    rl.CloseWindow()
}

BeginWindowTextureMode :: proc(w: GameWindow) {
    rl.BeginTextureMode(w.texture)
}
EndWindowTextureMode :: #force_inline proc() {
    rl.EndTextureMode()
}

DrawWindow :: proc(w: GameWindow) {
    rl.BeginDrawing()
    rl.ClearBackground(rl.BLACK)
    tex := w.texture.texture
    rl.DrawTexturePro(
        tex, 
        w.screenRect, 
        rl.Rectangle{0., 0., f32(rl.GetScreenWidth()), f32(rl.GetScreenHeight())}, 
        rl.Vector2{0., 0.}, 
        0., 
        rl.WHITE)
    rl.EndDrawing()
}
ScalingFactorX :: proc(w: GameWindow) -> f32 {
    return f32(w.width) / f32(rl.GetScreenWidth())
}
ScalingFactorY :: proc(w: GameWindow) -> f32 {
    return f32(w.height) / f32(rl.GetScreenHeight())
}